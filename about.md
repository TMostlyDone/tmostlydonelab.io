---
layout: posts
title: About
permalink: /about/
---

TMD is a recursive acronym which stands for "TMD's Mostly Done". It is a team made up of three people: [sseneca](https://ssene.ca), [Jonnobrow](https://jonnobrow.me) (Jonathan) and Matt. We attend hackathons together (#OpenBankhack18, Hack The South 2019) and partake in programming challenges (UKIPC 2018).
